#include<stdio.h>
char input()
{
	char a;
	scanf("%c",&a);
	return a;
}
int compute(char a)
{
	if((a=='a')||(a=='e')||(a=='i')||(a=='o')||(a=='u')||(a=='A')||(a=='E')||(a=='I')||(a=='O')||(a=='U'))
		return 1;
	else
		return 0;
}
void output(int a)
{
	if(a==1)
		printf("Entered character is a vowel ");
	else
		printf("Not a vowel");
}
void main()
{
	int a,large;
	printf("Enter a character : \n");
	a = input();
	large = compute(a);
	output(large);
}