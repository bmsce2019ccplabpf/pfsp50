#include<stdio.h>
#include<math.h>
#include<string.h>
int main()
{
	int x1,x2,y1,y2;
	float distance;
	printf("Enter the values of x1 and y1 coordinates of point 1\n");
	scanf("%d%d",&x1,&y1);
	printf("Enter the values of x2 and y2 coordinates of point 2\n");
	scanf("%d%d",&x2,&y2);
	distance=sqrt((pow((x2-x1),2))+(pow((y2-y1),2)));
	printf("distance between 2 points is %f\n",distance);
	return 0;
}